public class Ticket {
    // Tulis kode disini
    private String nama;
    private String asal;
    private String tujuan;

    // konstruktor default
    public Ticket(){
    } 

    //konstruktor kereta komuter
    public Ticket(String nama){
        this.nama = nama;
    }

    //konstruktor kereta kajj
    public Ticket(String nama, String asal, String tujuan){
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }
    
    //method menampilkan detail tiket
    public void detailTiket(){
        if(asal == null && tujuan == null){     //untuk mengecek jenis tiket (apabila tidak ada asal&tujuan maka termasuk komuter)
            System.out.println("Nama: "+nama);
        } else {                                //jika ada asal dan tujuan maka termasuk kajj jadi diprint nama, asal,tujuan
            System.out.println("Nama: "+nama);
            System.out.println("Asal: "+asal);
            System.out.println("Tujuan: "+tujuan);
            System.out.println("-------------------------------------");
        }
    }

}