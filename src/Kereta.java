public class Kereta {
    // Tulis kode disini
    private String namaKereta;
    private int tiketTersedia;
    private Ticket[] tiket;

    //constructor kereta komuter
    public Kereta(){
        this.namaKereta = "Komuter";
        this.tiketTersedia = 1000;
        tiket = new Ticket[this.tiketTersedia];
    }

    //constructor kereta KAJJ (menggunakan parameter nama dan jumlah tiket yang tersedia)
    public Kereta(String namaKereta, int tiketTersedia){
        this.namaKereta = namaKereta;
        this.tiketTersedia = tiketTersedia;
        tiket = new Ticket[this.tiketTersedia];
    }

    //method untuk menambah tiket kereta komuter di dalam array Ticket[] dengan parameter nama
    public void tambahTiket(String nama){ 
        if (tiketTersedia > 0){  // untuk mengecek apakah tiket masih ada
        this.tiketTersedia--;   // mengurangi jumlah tiket yang tellah dipesan
        for (int j=0;j<tiket.length;j++){  // untuk mengisi data array
            if (tiket[j] != null){          // untuk mengecek apakah di array sudah diisi
                continue;                   // jika sudah terisi maka continue/dilanjutkan
            } else {
                this.tiket[j] = new Ticket(nama);   
                break;
            }
        }
        System.out.println("==================================================");
        System.out.print("Tiket berhasil dipesan. ");
            if (tiketTersedia < 30 && tiketTersedia >= 0){  // untuk menampilkan sisa tiket jika urang dari 30 dan lebih dari 0
                System.out.print("Sisa tiket tersedia: "+tiketTersedia);
        }
        System.out.println();       // agar ===== berada dibawah kalimat
            }
            else{
                System.out.println("==================================================");
                System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
                }
    }

    //method untuk menambah tiket kereta komuter di dalam array ticket[] dengan parameter nama
    public void tambahTiket(String nama, String asal, String tujuan){
        if (tiketTersedia > 0){  // untuk mengecek apakah tiket masih ada
        this.tiketTersedia--;   // mengurangi jumlah tiket yang tellah dipesan
        for (int j=0;j<tiket.length;j++){   // untuk mengisi data array
            if (tiket[j] != null){      // untuk mengecek apakah di array sudah diisi
                continue;       // jika sudah terisi maka continue/dilanjutkan
            } else {
                tiket[j] = new Ticket(nama, asal, tujuan);
                break;
            }
        }
            System.out.println("==================================================");
            System.out.print("Tiket berhasil dipesan. ");
            
            if (tiketTersedia < 30 && tiketTersedia >= 0){      // untuk menampilkan sisa tiket jika urang dari 30 dan lebih dari 0
                System.out.print("Sisa tiket tersedia: "+tiketTersedia);
            }
            System.out.println();   // agar ===== berada dibawah kalimat
        }
            else {
            System.out.println("==================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }

    //method agar detail setap tiket di dalam array tiket[]
    public void tampilkanTiket(){
        System.out.println("==================================================");
        System.out.println("Daftar penumpang kereta api "+this.namaKereta+" : ");
        System.out.println("========================================");
        System.out.println("-------------------------------------");
        
        for (Ticket ticket : tiket){        
            if (ticket == null){    
                break;
            } else {
                ticket.detailTiket();
            }
        }
    }
}